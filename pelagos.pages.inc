<?php

/**
 * @file
 * Page callbacks for the pelagos module.
 */

/**
 * Constructs a descriptive page.
 */
function pelagos_description($pelagos_root = '/opt/pelagos', $namespace = NULL) {
  $markup = t('<p>Pelagos is a system for maintaining a repository of scientific research data.</p>');

  $items = array();
  foreach (_pelagos_get_categories($pelagos_root) as $category => $targets) {
    $url = 'pelagos/';
    if (isset($namespace) and !empty($namespace)) {
      $url .= "dev/$namespace/";
    }
    $url .= $category;
    $item = array('data' => $category);
    $item['children'] = array();
    foreach ($targets as $target) {
      $item['children'][] = array('data' => l($target, "$url/$target"));
    }
    $items[] = $item;
  }
  $markup .= theme(
    'item_list',
    array(
      'type' => 'ul',
      'title' => 'Components',
      'items' => $items,
      'attributes' => array('style' => 'height:auto'),
    )
  );

  if (!isset($namespace) or empty($namespace)) {
    $items = array();
    foreach (_pelagos_parse_aliases(variable_get('pelagos_aliases', '')) as $alias => $target) {
      $items[] = t(
        '!alias -> !target',
        array(
          '!alias' => l("/$alias", $alias),
          '!target' => l("/pelagos/$target", "pelagos/$target"),
        )
      );
    }
    if (count($items) > 0) {
      $markup .= theme(
        'item_list',
        array(
          'type' => 'ul',
          'title' => 'Aliases',
          'items' => $items,
          'attributes' => array('style' => 'height:auto'),
        )
      );
    }
  }

  // Items only shown in development mode.
  if (variable_get('pelagos_devel', 0)) {
    $pelagos_conf = '';
    if (isset($namespace) and !empty($namespace)) {
      $pelagos_conf .= t('<p><strong>Development Namespace:</strong> !namespace</p>', array('!namespace' => $namespace));
    }
    $pelagos_conf .= t('<p><strong>Pelagos Root:</strong> !pelagos_root</p>', array('!pelagos_root' => $pelagos_root));
    $branch = array();
    // Save the current working directory.
    $pelagos_orig_cwd = getcwd();
    // Change to the target driectory.
    chdir($pelagos_root);
    // Get the current git branch
    exec('git symbolic-ref --short HEAD', $branch);
    chdir($pelagos_orig_cwd);
    if (count($branch) > 0) {
      $branch_name = $branch[0];
    }
    else {
      $branch_name = 'unknown';
    }
    $pelagos_conf .= t(
      '<p><strong>Branch:</strong> !branch_name</p>',
      array(
        '!branch_name' => l(
          $branch_name,
          "https://triton.tamucc.edu/stash/projects/PELAGOS/repos/pelagos/browse?at=refs/heads/$branch_name",
          array('attributes' => array('target' => '_blank'))
        )
      )
    );
    $pelagos_conf .= t(
      '<p><strong>Development Mode:</strong> enabled <span style="font-size:x-small">(!dev)</span></p>',
      array('!dev' => l(t('Development Area'), 'pelagos/dev'))
    );
    if (!isset($namespace) or empty($namespace)) {
      $pelagos_conf .= _pelagos_get_devel_namespaces_markup();
    }
    $markup .= theme(
      'fieldset',
      array(
        'element' => array(
          '#title' =>
          t('Pelagos Drupal Module Configuration') . ' ' .
          t(
            '<span style="font-size:x-small">(!pelagos_conf_menu)</span>',
            array('!pelagos_conf_menu' => l(t('configure'), 'admin/config/system/pelagos'))
          ),
          '#children' => $pelagos_conf,
        ),
      )
    );
  }

  return array(
    'pelagos_description' => array(
      '#theme' => 'container',
      '#markup' => $markup,
    ),
  );
}

/**
 * Constructs a descriptive page for dev.
 */
function pelagos_dev_description() {
  $markup = t('<p>This area is for Pelagos development.</p>');
  $markup .= t('<p>Below are the development namespaces defined for this instance.</p>');
  $markup .= t(
    '<p>Development Namespaces are configured in the !pelagos_conf_menu.</p>',
    array('!pelagos_conf_menu' => l(t('Pelagos Configuration Menu'), 'admin/config/system/pelagos'))
  );
  $markup .= _pelagos_get_devel_namespaces_markup();
  return array(
    'pelagos_dev_description' => array(
      '#theme' => 'container',
      '#markup' => $markup,
    ),
  );
}

/**
 * Returns markup for the development namespaces.
 */
function _pelagos_get_devel_namespaces_markup() {
  $namespaces = _pelagos_parse_devel_namespaces(variable_get('pelagos_devel_namespaces', ''));
  if (count($namespaces) == 0) {
    $namespace_table = 'No development namespaces defined.';
  }
  else {
    foreach ($namespaces as $namespace => $namespace_pelagos_root) {
      $namespace = trim($namespace);
      $namespace_pelagos_root = trim($namespace_pelagos_root);
      $branch = array();
      // Save the current working directory.
      $pelagos_orig_cwd = getcwd();
      // Change to the target driectory.
      chdir($namespace_pelagos_root);
      // Get the current git branch
      exec('git symbolic-ref --short HEAD', $branch);
      chdir($pelagos_orig_cwd);
      if (count($branch) > 0) {
        $branch_name = $branch[0];
      }
      else {
        $branch_name = 'unknown';
      }
      $rows[] = array(
        array(
          'data' => t(
            '!namespace',
            array('!namespace' => l($namespace, "pelagos/dev/$namespace"))
          ),
        ),
        array('data' => $namespace_pelagos_root),
        array(
          'data' => t(
            '!branch_name',
            array(
              '!branch_name' => l(
                $branch_name,
                "https://triton.tamucc.edu/stash/projects/PELAGOS/repos/pelagos/browse?at=refs/heads/$branch_name",
                array('attributes' => array('target' => '_blank'))
              )
            )
          ),
        ),
      );
    }
    $namespace_table = theme(
      'table',
      array(
        'attributes' => array('style' => 'width:auto;'),
        'header' => array(
          array('data' => t('Namespace')),
          array('data' => t('Pelagos Root')),
          array('data' => t('Branch')),
        ),
        'rows' => $rows,
      )
    );
  }
  return theme(
    'fieldset',
    array(
      'element' => array(
        '#title' => 'Development Namespaces:',
        '#children' => $namespace_table,
      ),
    )
  );
}
