<?php

/**
 * @file
 * Exec callbacks for the pelagos module.
 */

/**
 * Executes a Pelagos application or module.
 */
function pelagos_static($target_category, $target, $target_path = 'pelagos', $pelagos_root = '/opt/pelagos') {
  drupal_add_css('.pelagos_static_dir { font-weight: bold; }', array('type' => 'inline'));
  drupal_add_css('.pelagos_static_files { padding-left: 1em; }', array('type' => 'inline'));
  // Strip everthing after slash from target (in case it includes a route).
  $target = preg_replace('!/.*$!', '', $target);
  // Parse out the static file part from the REQUEST_URI.
  if (preg_match('!^' . base_path() . "$target_path/static(.*)$!", $_SERVER['REQUEST_URI'], $matches)) {
    // Strip preceding slashes.
    $file_part = preg_replace('!^/+!', '', $matches[1]);
    // Strip trailing slashes.
    $file_part = preg_replace('!/+$!', '', $file_part);
    // Don't allow any . or .. trickery.
    if (preg_match('!^[\./]+$!', $file_part)) {
      return MENU_NOT_FOUND;
    }
    $file = "$pelagos_root/web/$target_category/$target/static/$file_part";
    if (file_exists($file)) {
      if (is_dir($file)) {
        $dh = opendir("$pelagos_root/web/$target_category/$target/static/$file_part");
        $output = "<div class='pelagos_static_dir'>$target_path/static";
        if (!empty($file_part)) {
          $output .= "/$file_part";
        }
        $output .= "</div><div class='pelagos_static_files'>";
        while (FALSE !== ($filename = readdir($dh))) {
          if (preg_match('/^\./', $filename)) {
            continue;
          }
          $link = "$target_path/static/";
          if (!empty($file_part)) {
            $link .= "$file_part/";
          }
          $link .= $filename;
          $output .= t(
            '!file_link<br />',
            array('!file_link' => l($filename, $link))
          );
        }
        $output .= '</div>';
        $target_name = $target_category;
        if (!empty($target)) {
          $target_name .= "/$target";
        }
        return array('#markup' => $output);
      }
      if (preg_match('/\.css$/', $file)) {
        $mime = 'text/css';
      }
      elseif (preg_match('/\.js$/', $file)) {
        $mime = 'text/javascript';
      }
      else {
        $info = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($info, $file);
      }
      if ($mime === FALSE) {
        return MENU_NOT_FOUND;
      }
      header('Content-Length: ' . filesize($file));
      header('Content-Disposition: inline; filename=' . basename($file));
      header('Content-Transfer-Encoding: binary');
      header('Content-type: ' . $mime);
      flush();
      ob_clean();
      readfile($file);
      drupal_exit();
    }
    return MENU_NOT_FOUND;
  }
  return MENU_NOT_FOUND;
}
