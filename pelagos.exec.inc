<?php

/**
 * @file
 * Exec callbacks for the pelagos module.
 */

/**
 * Executes a Pelagos application or module.
 */
function pelagos_exec($category, $target, $target_path, $pelagos_root = '/opt/pelagos') {
  // Save original environment.
  $orig_env = array();
  $orig_env['SCRIPT_NAME'] = $_SERVER['SCRIPT_NAME'];
  $orig_env['QUERY_STRING'] = $_SERVER['QUERY_STRING'];
  $orig_env['REQUEST_URI'] = $_SERVER['REQUEST_URI'];

  // Separate target and target route.
  preg_match('!([^/]*)(.*?)$!', $target, $matches);
  $target = $matches[1];
  $target_route = $matches[2];

  // Modify environment for the target.
  // Fix up REQUEST_URI.
  // Grab extra path information.
  $extra_path = preg_replace('!^' . base_path() . $target_path . '!', '', $_SERVER['REQUEST_URI']);
  // Start rebuilding REQUEST_URI with Drupal base path + target path.
  $_SERVER['REQUEST_URI'] = base_path() . $target_path;
  if (isset($target_route) and !empty($target_route)) {
    // If there's a target route (e.g. as specified by an alias) append it.
    $_SERVER['REQUEST_URI'] .= $target_route;
  }
  if (isset($extra_path) and !empty($extra_path)) {
    // If there was extra path info, append it.
    $_SERVER['REQUEST_URI'] .= $extra_path;
  }
  // Fix up SCRIPT_NAME.
  $_SERVER['SCRIPT_NAME'] = base_path() . $target_path;
  // Fix up QUERY_STRING.
  $_SERVER['QUERY_STRING'] = preg_replace('/^q=[^&]+&?/', '', $_SERVER['QUERY_STRING']);
  // Initialize target found flag.
  $pelagos_target_found = FALSE;
  // Initialize output.
  $output = '';

  // Define Pelagos global.
  $GLOBALS['pelagos'] = array(
    'root' => $pelagos_root,
    'base_path' => base_path() . 'pelagos',
    'base_url' => $GLOBALS['base_url'] . base_path() . 'pelagos',
    'component_path' => base_path() . $target_path,
    'component_url' => $GLOBALS['base_url'] . base_path() . $target_path,
  );

  if (preg_match('!pelagos(/dev/[^/]+)!', $target_path, $matches)) {
    // Add develepment namespace to base_path if we are in one.
    $GLOBALS['pelagos']['base_path'] .= $matches[1];
  }

  $branch = array();

  if (file_exists("$pelagos_root/web/$category/$target/index.php")) {
    // Save the current working directory.
    $pelagos_orig_cwd = getcwd();
    // Change to the target driectory.
    chdir("$pelagos_root/web/$category/$target");
    if (variable_get('pelagos_devel', 0)) {
      // Get the current git branch
      exec('git symbolic-ref --short HEAD', $branch);
    }

    // Prepend Pelagos php include path.
    set_include_path("$pelagos_root/share/php" . PATH_SEPARATOR . get_include_path());

    // If pelagos has an AutoLoader.
    if (file_exists("$pelagos_root/share/php/Pelagos/AutoLoader.php")) {
      // Include AutoLoader.
      require_once 'Pelagos/AutoLoader.php';
      // Instatiate the loader.
      $loader = new \Pelagos\AutoLoader;
      // Register it.
      $loader->register();
      // Add the Pelagos namespace.
      $loader->addNamespace('Pelagos', "$pelagos_root/share/php/Pelagos");
    }

    // Start buffering output.
    ob_start();
    $time_start = microtime(TRUE);
    // Run the target.
    require_once 'index.php';
    $time_end = microtime(TRUE);
    $exec_time = $time_end - $time_start;
    // Get the buffered output, clear the buffer, and stop buffering.
    $output = ob_get_clean();
    // Restore the original working directory.
    chdir($pelagos_orig_cwd);
    // Flag that the target was found.
    $pelagos_target_found = TRUE;
  }

  if (array_key_exists('pelagos', $GLOBALS)) {
    if (array_key_exists('title', $GLOBALS['pelagos'])) {
      // Set the page title as specified by the target.
      drupal_set_title($GLOBALS['pelagos']['title']);
    }
    if (array_key_exists('show_title', $GLOBALS['pelagos'])) {
      // Hide the page title if specified by the target.
      $GLOBALS['hide_page_title'] = !$GLOBALS['pelagos']['show_title'];
    }
  }

  // Restore the original environment.
  foreach ($orig_env as $key => $val) {
    $_SERVER[$key] = $val;
  }

  if ($pelagos_target_found) {
    $module_path = drupal_get_path('module', 'pelagos');
    $build = array(
      '#attached' => array(
        'css' => array(
          $module_path . '/pelagos-module.css',
        ),
      ),
    );
    $build['pelagos-container'] = array(
      '#type' => 'container',
      '#attributes' => array('id' => 'pelagos-container'),
      'container-row' => array(
        '#type' => 'container',
        '#attributes' => array('id' => 'pelagos-container-row'),
        'container-cell' => array(
          '#type' => 'container',
          '#attributes' => array('id' => 'pelagos-container-cell'),
          'wrapper' => array(
            '#type' => 'container',
            '#attributes' => array('id' => 'pelagos-wrapper'),
            'wrapper-row' => array(
              '#type' => 'container',
              '#attributes' => array('id' => 'pelagos-wrapper-row'),
              'pelagos-content' => array(
                '#type' => 'container',
                '#attributes' => array('id' => 'pelagos-content'),
                '#children' => $output,
              ),
            ),
          ),
        ),
      ),
    );
    if (variable_get('pelagos_devel', 0)) {
      $markup = '$GLOBALS[pelagos]:';
      $markup .= " ( root => '" . $GLOBALS['pelagos']['root'] .
        "', base_path => '" . $GLOBALS['pelagos']['base_path'] .
        "', component_path => '" . $GLOBALS['pelagos']['component_path'] . "' )" .
        " &nbsp;Time: " . round($exec_time, 3) . " sec";
        if (count($branch) > 0) {
          $markup .= " &nbsp;Branch: $branch[0]";
        }
      $build['pelagos-dev-toolbar'] = array(
        '#theme' => 'container',
        '#access' => user_access('develop pelagos'),
        '#attributes' => array('id' => 'pelagos-dev-toolbar'),
        '#markup' => $markup,
      );
    }
    return $build;
  }
  else {
    return MENU_NOT_FOUND;
  }
}
