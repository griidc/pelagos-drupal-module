<?php

/**
 * @file
 * Pelagos module settings UI.
 */

/**
 * Callback for Pelagos admin settings.
 */
function pelagos_admin_settings() {
  $form = array();

  $form['pelagos_root'] = array(
    '#type' => 'textfield',
    '#title' => t('Pelagos Root'),
    '#default_value' => variable_get('pelagos_root', '/opt/pelagos'),
    '#size' => 50,
    '#maxlength' => 255,
    '#description' => t('The root directory of the Pelagos installation.'),
    '#required' => TRUE,
  );

  $form['pelagos_auth_type'] = array(
    '#type' => 'select',
    '#title' => t('Authentication Type'),
    '#options' => array('drupal' => 'Drupal', 'cas' => 'CAS'),
    '#default_value' => variable_get('pelagos_auth_type', 'drupal'),
    '#description' => t('The type of authentication to use.'),
    '#required' => TRUE,
  );

  $form['pelagos_aliases'] = array(
    '#type' => 'textarea',
    '#title' => t('Aliases'),
    '#default_value' => variable_get('pelagos_aliases', ''),
    '#rows' => 5,
    '#description' => t('Aliases (e.g. alias:category/target).'),
  );

  // Developer settings.
  $form['devel'] = array(
    '#type' => 'fieldset',
    '#title' => t('Development Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['devel']['pelagos_devel'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable development mode'),
    '#description' => t('Check this to enable development mode.'),
    '#default_value' => variable_get('pelagos_devel', 0),
  );

  $form['devel']['pelagos_devel_namespaces'] = array(
    '#type' => 'textarea',
    '#title' => t('Development Namespaces'),
    '#default_value' => variable_get('pelagos_devel_namespaces', ''),
    '#rows' => 5,
    '#description' => t('Development namespaces (e.g. namespace:/path/to/pelagos/root).'),
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'pelagos_admin_settings_after_submit';
  return $form;
}

/**
 * Validator for Pelagos admin settings.
 */
function pelagos_admin_settings_validate($form, &$form_state) {
  // Trim any whitepsace.
  $form_state['values']['pelagos_root'] = trim($form_state['values']['pelagos_root']);
  // Remove trailing /.
  $form_state['values']['pelagos_root'] = preg_replace('/\/+$/', '', $form_state['values']['pelagos_root']);
  // Make sure directory exists.
  if (!file_exists($form_state['values']['pelagos_root'])) {
    form_set_error(
      'pelagos_root',
      t("Pelagos Root directory '") .
      $form_state['values']['pelagos_root'] .
      t("' does not exist.")
    );
  }
  // Make sure it looks like a pelagos installation.
  elseif (!file_exists($form_state['values']['pelagos_root'] . '/web/applications')) {
    form_set_error(
      'pelagos_root',
      t("Pelagos Root directory '") .
      $form_state['values']['pelagos_root'] .
      t("' does not look like a Pelagos installation.")
    );
  }
  try {
    $aliases = _pelagos_parse_aliases($form_state['values']['pelagos_aliases']);
    foreach ($aliases as $alias => $target) {
      // Extract target category and target.
      preg_match('!([^/]+)/([^/]+)!', $target, $matches);
      $target_category = $matches[1];
      $target = $matches[2];
      if (!file_exists(variable_get('pelagos_root', '/opt/pelagos') . "/web/$target_category/$target/index.php")) {
        throw new Exception("Alias '$alias' has invalid target: $target_category/$target");
      }
    }
  }
  catch (Exception $e) {
    form_set_error('pelagos_aliases', $e->getMessage());
  }
  if ($form_state['values']['pelagos_devel']) {
    try {
      $namespaces = _pelagos_parse_devel_namespaces($form_state['values']['pelagos_devel_namespaces']);
      foreach ($namespaces as $namespace => $namespace_pelagos_root) {
        $namespace = trim($namespace);
        $namespace_pelagos_root = trim($namespace_pelagos_root);
        if (!file_exists($namespace_pelagos_root)) {
          throw new Exception(
            "Development Namespace '$namespace' has an invalid Pelagos Root: " .
            "$namespace_pelagos_root (directory does not exist)."
          );
        }
        elseif (!file_exists("$namespace_pelagos_root/web")) {
          throw new Exception(
            "Development Namespace '$namespace' has an invalid Pelagos Root: " .
            "$namespace_pelagos_root (does not look like a Pelagos instance)."
          );
        }
      }
    }
    catch (Exception $e) {
      form_set_error('pelagos_devel_namespaces', $e->getMessage());
    }
  }
}

/**
 * Additional function to call after submit of Pelagos admin settings.
 */
function pelagos_admin_settings_after_submit($form, &$form_state) {
  drupal_flush_all_caches();
}
